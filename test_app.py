#!/bin/bash python
import unittest
import app
from app import hello

class TestApp(unittest.TestCase):
    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()

    def test_hello(self):
        self.assertTrue(hello() == "Hello")


if __name__ == "__main__":
    import xmlrunner
    runner = xmlrunner.XMLTestRunner(output='reports')
    unittest.main(testRunner=runner)
