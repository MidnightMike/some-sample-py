FROM python:3-alpine

ADD . /code
WORKDIR /code

RUN pip install --upgrade pip
RUN pip3 install -r requirements.txt

EXPOSE 5000

CMD python app.py
